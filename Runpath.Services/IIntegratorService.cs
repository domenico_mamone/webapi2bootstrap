﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Runpath.Domain.Model;

namespace Runpath.Services
{
    public interface IIntegratorService
    {
        List<IntegrationModel> FilterByUSerId(List<IntegrationModel> integration, int userId);
        List<IntegrationModel> MapDataToIntegrationModel(List<Photo> photos, List<Album> albums);
        Task<List<IntegrationModel>> GetFilteredData(int userId);
    }
}
