﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Runpath.Domain.Model;
using Newtonsoft.Json;

namespace Runpath.Services
{
    public class IntegratorService : IIntegratorService
    {
        private readonly Dictionary<string, string> endpoints;

        public IntegratorService(Dictionary<string, string> endpoints)
        {
            this.endpoints = endpoints;
        }

        private List<IntegrationModel> FilteredData { get; set; }

        /// <summary>
        /// Filter data by user id
        /// </summary>
        /// <param name="integration"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<IntegrationModel> FilterByUSerId(List<IntegrationModel> integration, int userId)
        {
            return integration.Where(x => x.userId == userId).ToList();
        }

        /// <summary>
        /// Maps photos and albums fields and returns a new integration model 
        /// </summary>
        /// <param name="photos"></param>
        /// <param name="albums"></param>
        /// <returns></returns>
        public List<IntegrationModel> MapDataToIntegrationModel(List<Photo> photos, List<Album> albums)
        {
            return (from p in photos
                    join a in albums on p.albumId equals a.id
                    select new IntegrationModel()
                    {
                        userId = a.userId,
                        albumTitle = a.title,
                        photoTitle = p.title,
                        thumbnailUrl = p.thumbnailUrl,
                        url = p.url

                    }).ToList();
        }


        /// <summary>
        /// Http call to endpoints and returns filtered data in JSON format 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<List<IntegrationModel>> GetFilteredData(int userId)
        {

            using (var client = new HttpClient())
            {

                var photosBaseAddress = client.BaseAddress = new Uri(endpoints["photos"]);
                var albumsBaseAddress = client.BaseAddress = new Uri(endpoints["albums"]);

                using var responsePhotoRequest = await client.GetAsync(photosBaseAddress);
                using var responseAlbumRequest = await client.GetAsync(albumsBaseAddress);
                string responseBodyPhoto = await responsePhotoRequest.Content.ReadAsStringAsync();
                string responseBodyAlbum = await responseAlbumRequest.Content.ReadAsStringAsync();
                var photoDeserialized = JsonConvert.DeserializeObject<List<Photo>>(responseBodyPhoto);
                var albumDeserialized = JsonConvert.DeserializeObject<List<Album>>(responseBodyAlbum);

                var mappedData = MapDataToIntegrationModel(photoDeserialized, albumDeserialized);
                FilteredData = FilterByUSerId(mappedData, userId);
            }

            return FilteredData;

        }

    }
}