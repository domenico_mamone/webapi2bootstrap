﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Runpath.Services;

namespace Runpath.Controllers
{
    [ApiController]
    [Route("api/v1/runpath")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public class IntegrationController : ControllerBase
    {
        private readonly ILogger<IntegrationController> _logger;
        private readonly IIntegratorService _integratorService;
        private readonly IConfiguration _configuration;

        public IntegrationController(ILogger<IntegrationController> logger, IIntegratorService integratorService, IConfiguration configuration)
        {
            _logger = logger;
            _integratorService = integratorService;
            _configuration = configuration;
        }

        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> Get(string id)
        {
            try
            {
                var convert = Int32.TryParse(id, out int userId);
                if (!convert)
                {
                    _logger.LogError($"ERROR: Get. Wrong Id {id} ");
                }

                var filteredData = await _integratorService.GetFilteredData(userId);
                return Ok( filteredData );

            }
            catch (Exception ex)
            {
                _logger.LogError($"ERROR: Get {ex}");
                return StatusCode(500, "Error");

            }
        }
    }
}
