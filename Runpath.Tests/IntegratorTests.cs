using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using Runpath.Domain.Model;
using Runpath.Services;
using Xunit;

namespace Runpath.Tests
{
    // TODO: improve testing
    // rename test methods appropriately
    // add more test coverage
    public class IntegratorTests
    {

        [Fact()]
        public void test_json_deserializes_correctly()
        {
            var photos = JsonConvert.DeserializeObject<List<Photo>>(Data.Photos);
            var albums = JsonConvert.DeserializeObject<List<Album>>(Data.Albums);
            Assert.NotNull(photos);
            Assert.NotNull(albums);
        }


        // Allows an integrator to filter on the user id – so just returns the albums and photos relevant to a single user.
        [Fact()]
        public void test_json_integration()
        {

            var photos = JsonConvert.DeserializeObject<List<Photo>>(Data.Photos);
            var albums = JsonConvert.DeserializeObject<List<Album>>(Data.Albums);

            var integrationService = new IntegratorService();
            List<IntegrationModel> integration = integrationService.MapDataToIntegrationModel(photos, albums).Take(10).ToList();

            Assert.NotNull(integration);


        }


        [Fact()]
        public void test_filter_by_user_id()
        {
            var photos = JsonConvert.DeserializeObject<List<Photo>>(Data.Photos);
            var albums = JsonConvert.DeserializeObject<List<Album>>(Data.Albums);
            var userId = 1;

            var integrationService = new IntegratorService();
            List<IntegrationModel> integration = integrationService.MapDataToIntegrationModel(photos, albums);
            List<IntegrationModel> selectByUserId = integrationService.FilterByUSerId(integration, userId);

            Assert.NotEmpty(selectByUserId);

        }


    }
}
