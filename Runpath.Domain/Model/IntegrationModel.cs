﻿using System;
using System.Collections.Generic;

namespace Runpath.Domain.Model
{
    public class IntegrationModel
    {
        public int userId { get; set; }
        public string albumTitle { get; set; }
        public string photoTitle { get; set; }
        public string url { get; set; }
        public string thumbnailUrl { get; set; }

    }
}
